<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (C) 2015 The Android Open Source Project
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License
  -->

<manifest xmlns:android="http://schemas.android.com/apk/res/android"
     xmlns:androidprv="http://schemas.android.com/apk/prv/res/android"
     package="com.android.traceur"
     android:versionCode="2"
     android:versionName="1.0">
    <uses-sdk android:minSdkVersion="26"
         android:targetSdkVersion="35"/>

    <!--- Used to query for Betterbug. -->
    <uses-permission android:name="android.permission.QUERY_ALL_PACKAGES"/>

    <!--- Used to save ViewCapture data via LauncherAppsService. -->
    <uses-permission android:name="android.permission.READ_FRAME_BUFFER"/>

    <!-- Used to trigger the trace start/stop service. -->
    <uses-permission android:name="android.permission.START_FOREGROUND_SERVICES_FROM_BACKGROUND"/>

    <!-- Used for receiving BUGREPORT_STARTED intents. -->
    <uses-permission android:name="android.permission.DUMP"/>

    <!-- Used to initialize tracing on boot. -->
    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED"/>

    <!-- Used to prevent non-system windows from displaying over Traceur. -->
    <uses-permission android:name="android.permission.HIDE_NON_SYSTEM_OVERLAY_WINDOWS"/>

    <!-- Used for adding the Quick Settings tile to the status bar. -->
    <uses-permission android:name="android.permission.STATUS_BAR"/>

    <!-- Used for brief periods where the trace service is foregrounded. -->
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE"/>
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE_SPECIAL_USE"/>

    <!-- Used to post file-sending notification. -->
    <uses-permission android:name="android.permission.POST_NOTIFICATIONS" />

    <!-- Used to check that the current user is an admin user. -->
    <uses-permission android:name="android.permission.QUERY_USERS" />

    <!-- Used to control IME Winscope tracing (not yet migrated to perfetto). -->
    <uses-permission android:name="android.permission.CONTROL_UI_TRACING" />

    <!-- Used to control view capture Winscope tracing (not yet migrated to perfetto). -->
    <uses-permission android:name="android.permission.WRITE_SECURE_SETTINGS" />

    <!-- Used to get a list of apps for heap dumps. -->
    <uses-permission android:name="android.permission.REAL_GET_TASKS" />

    <!-- Declare Android TV support. -->
    <uses-feature android:name="android.software.leanback"
         android:required="false"/>

    <!-- touch screen is not required for TV -->
    <uses-feature android:name="android.hardware.touchscreen"
         android:required="false"/>

    <application android:label="@string/system_tracing"
         android:banner="@drawable/banner"
         android:icon="@drawable/ic_launcher_settings"
         android:backupAgent="com.android.traceur.TraceurBackupAgent">

        <activity android:name=".MainActivity"
             android:description="@string/record_system_activity"
             android:label="@string/system_tracing"
             android:theme="@style/Theme.SubSettingsBase"
             android:launchMode="singleTop"
             android:exported="true">
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.INFO"/>
            </intent-filter>
            <intent-filter>
                <action android:name="android.service.quicksettings.action.QS_TILE_PREFERENCES"/>
            </intent-filter>

            <!-- Mark this activity as a dynamic setting in the development category. -->
            <intent-filter>
                <action android:name="com.android.settings.action.IA_SETTINGS"/>
            </intent-filter>
            <meta-data android:name="com.android.settings.category"
                 android:value="com.android.settings.category.ia.development"/>
            <meta-data android:name="com.android.settings.summary"
                 android:resource="@string/record_system_activity"/>
            <meta-data android:name="com.android.settings.order"
                 android:value="10"/>
            <meta-data android:name="com.android.settings.profile"
                 android:value="primary_profile_only"/>
        </activity>

        <activity android:name=".MainTvActivity"
             android:description="@string/record_system_activity"
             android:label="@string/system_tracing"
             android:theme="@style/TvTheme"
             android:launchMode="singleTop"
             android:exported="true">
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.LEANBACK_LAUNCHER"/>
                <category android:name="android.intent.category.INFO"/>
            </intent-filter>
        </activity>

        <activity android:name=".MainWearActivity"
                  android:description="@string/record_system_activity"
                  android:icon="@drawable/bugfood_icon"
                  android:label="@string/system_tracing"
                  android:theme="@style/WearTheme"
                  android:launchMode="singleTask"
                  android:exported="true">
        </activity>

        <activity android:name=".UserConsentActivityDialog"
             android:theme="@android:style/Theme.DeviceDefault.Light.Dialog.Alert"
             android:finishOnCloseSystemDialogs="true"
             android:excludeFromRecents="true"
             android:exported="false"/>

        <receiver android:name=".Receiver"
             android:permission="android.permission.DUMP"
             android:exported="true">
            <intent-filter android:priority="2147483647">
                <action android:name="android.intent.action.BOOT_COMPLETED"/>
                <action android:name="android.intent.action.USER_FOREGROUND"/>
                <action android:name="com.android.internal.intent.action.BUGREPORT_STARTED"/>
            </intent-filter>
          </receiver>

        <service android:name=".StopTraceService"
             android:exported="true"
             android:foregroundServiceType="specialUse">
            <property android:name="android.app.PROPERTY_SPECIAL_USE_FGS_SUBTYPE"
                 android:value="Used for ensuring that Traceur isn't killed while starting or stopping traces."/>
        </service>

        <service android:name=".BindableTraceService"
            android:permission="android.permission.CONTROL_UI_TRACING"
            android:exported="true"/>

        <service android:name=".TraceService"
             android:exported="false"
             android:foregroundServiceType="specialUse">
            <property android:name="android.app.PROPERTY_SPECIAL_USE_FGS_SUBTYPE"
                 android:value="Used for ensuring that Traceur isn't killed while starting or stopping traces."/>
        </service>

        <service android:name=".TracingQsService"
             android:enabled="false"
             android:icon="@drawable/bugfood_icon"
             android:label="@string/record_trace"
             android:permission="android.permission.BIND_QUICK_SETTINGS_TILE"
             android:exported="true">
            <intent-filter>
                <action android:name="android.service.quicksettings.action.QS_TILE"/>
            </intent-filter>
            <meta-data android:name="android.service.quicksettings.TOGGLEABLE_TILE"
                       android:value="true" />
        </service>

        <service android:name=".StackSamplingQsService"
             android:enabled="false"
             android:icon="@drawable/bugfood_icon"
             android:label="@string/record_stack_samples"
             android:permission="android.permission.BIND_QUICK_SETTINGS_TILE"
             android:exported="true">
            <intent-filter>
                <action android:name="android.service.quicksettings.action.QS_TILE"/>
            </intent-filter>
            <meta-data android:name="android.service.quicksettings.TOGGLEABLE_TILE"
                       android:value="true" />
        </service>

        <provider android:name="androidx.core.content.FileProvider"
             android:authorities="com.android.traceur.files"
             android:grantUriPermissions="true"
             android:exported="false">
            <meta-data android:name="android.support.FILE_PROVIDER_PATHS"
                 android:resource="@xml/file_paths"/>
        </provider>

        <provider android:name=".StorageProvider"
             android:enabled="false"
             android:authorities="com.android.traceur.documents"
             android:grantUriPermissions="true"
             android:exported="true"
             android:permission="android.permission.MANAGE_DOCUMENTS">
            <intent-filter>
              <action android:name="android.content.action.DOCUMENTS_PROVIDER"/>
            </intent-filter>
        </provider>

        <provider android:name=".SearchProvider"
             android:authorities="com.android.traceur"
             android:multiprocess="false"
             android:grantUriPermissions="true"
             android:permission="android.permission.READ_SEARCH_INDEXABLES"
             android:exported="true">
            <intent-filter>
                <action android:name="android.content.action.SEARCH_INDEXABLES_PROVIDER"/>
            </intent-filter>
        </provider>
    </application>
</manifest>
